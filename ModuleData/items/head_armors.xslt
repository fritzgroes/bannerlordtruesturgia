<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes"/>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Item[@id='spangenhelm_with_padded_cloth']/@weight">
    <xsl:attribute name="weight">1.8</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='spangenhelm_with_padded_cloth']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">12</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='spangenhelm_with_leather']/@weight">
    <xsl:attribute name="weight">2.0</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='spangenhelm_with_leather']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">14</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='nasal_helmet']/@weight">
    <xsl:attribute name="weight">1.8</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='nasal_helmet']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">16</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgian_helmet_base']/@weight">
    <xsl:attribute name="weight">2.0</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='sturgian_helmet_base']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">18</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='nasalhelm_over_leather']/@weight">
    <xsl:attribute name="weight">2.4</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='nasalhelm_over_leather']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">22</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='nasalhelm_over_mail']/@weight">
    <xsl:attribute name="weight">2.6</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='nasalhelm_over_mail']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">24</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='nasal_helmet_with_leather']/@weight">
    <xsl:attribute name="weight">2.2</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='nasal_helmet_with_leather']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">26</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='nasal_helmet_with_mail']/@weight">
    <xsl:attribute name="weight">2.5</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='nasal_helmet_with_mail']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">28</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgia_heavy_cavalary_helmet']/@weight">
    <xsl:attribute name="weight">3.2</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='sturgia_heavy_cavalary_helmet']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">30</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='nordic_helmet']/@weight">
    <xsl:attribute name="weight">3.1</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='nordic_helmet']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">30</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='northern_goggled_helmet']/@weight">
    <xsl:attribute name="weight">3.1</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='northern_goggled_helmet']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">32</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='goggled_helmet_over_leather']/@weight">
    <xsl:attribute name="weight">3.1</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='goggled_helmet_over_leather']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">32</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='goggled_helmet_over_mail']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">34</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgian_helmet_open']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">35</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgian_helmet_b_open']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">36</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgian_crown']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">41</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgian_helmet_closed']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">50</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgian_helmet_b_close']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">51</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='lendman_helmet_over_mail']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">44</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='northern_warlord_helmet']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">44</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='lendman_helmet_over_full_mail']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">47</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgian_battle_crown']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">46</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgian_lord_helmet_a']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">46</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgian_lord_helmet_b']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">48</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='closed_goggled_helmet']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">48</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='goggled_helmet_over_full_mail']/@weight">
    <xsl:attribute name="weight">4.0</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='goggled_helmet_over_full_mail']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">50</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='decorated_goggled_helmet']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">52</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='sturgian_lord_helmet_c']/ItemComponent/Armor/@head_armor">
    <xsl:attribute name="head_armor">54</xsl:attribute>
  </xsl:template>
</xsl:stylesheet>