
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes"/>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Item[@id='leather_and_iron_plate_armor']/ItemComponent/Armor/@arm_armor">
    <xsl:attribute name="arm_armor">6</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='leather_and_iron_plate_armor']/ItemComponent/Armor/@leg_armor">
    <xsl:attribute name="leg_armor">6</xsl:attribute>
  </xsl:template>
</xsl:stylesheet>