<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes"/>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Item[@id='battania_civil_cape']/@weight">
    <xsl:attribute name="weight">1.5</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='brass_lamellar_shoulder_white']/@weight">
    <xsl:attribute name="weight">3.0</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='brass_lamellar_shoulder_white']/ItemComponent/Armor/@body_armor">
    <xsl:attribute name="body_armor">18</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='a_brass_lamellar_shoulder_white_a']/@weight">
    <xsl:attribute name="weight">1.5</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='a_brass_lamellar_shoulder_white_a']/ItemComponent/Armor/@arm_armor">
    <xsl:attribute name="body_armor">6</xsl:attribute>
  </xsl:template>

  <xsl:template match="Item[@id='a_brass_lamellar_shoulder_white_b']/@weight">
    <xsl:attribute name="weight">2.0</xsl:attribute>
  </xsl:template>
  <xsl:template match="Item[@id='a_brass_lamellar_shoulder_white_b']/ItemComponent/Armor">
    <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
        <xsl:attribute name="body_armor">8</xsl:attribute>
    </xsl:copy> 
  </xsl:template>
  <xsl:template match="Item[@id='a_brass_lamellar_shoulder_white_b']/ItemComponent/Armor/@arm_armor">
    <xsl:attribute name="arm_armor">4</xsl:attribute>
  </xsl:template>
</xsl:stylesheet>