<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes"/>
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="NPCCharacter[@id='sturgian_recruit']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_axe_2_t2" />
                <equipment slot="Body" id="Item.tundra_tunic" />
                <equipment slot="Cape" id="Item.wrapped_scarf" />
                <equipment slot="Leg" id="Item.sturgia_boots_d" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_axe_2_t2" />
                <equipment slot="Body" id="Item.fur_trimmed_short_tunic" />
                <equipment slot="Gloves" id="Item.leather_gloves" />
                <equipment slot="Leg" id="Item.sturgia_boots_c" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.peasant_hammer_2_t1" />
                <equipment slot="Body" id="Item.sturgia_light_tunic" />
                <equipment slot="Gloves" id="Item.ragged_armwraps" />
                <equipment slot="Cape" id="Item.wrapped_scarf" />
                <equipment slot="Leg" id="Item.sturgia_boots_d" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.peasant_sickle_1_t1" />
                <equipment slot="Body" id="Item.light_tunic" />
                <equipment slot="Cape" id="Item.scarf" />
                <equipment slot="Gloves" id="Item.armwraps" />
                <equipment slot="Leg" id="Item.sturgia_boots_c" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t1" civilian="true" />
        </Equipments> 
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_warrior']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_1_t2" />
                <equipment slot="Item1" id="Item.northern_round_shield" />
                <equipment slot="Head" id="Item.northern_fur_cap" />
                <equipment slot="Cape" id="Item.wrapped_scarf" />
                <equipment slot="Body" id="Item.layered_leather_tunic" />
                <equipment slot="Gloves" id="Item.leather_gloves" />
                <equipment slot="Leg" id="Item.sturgia_boots_a" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_axe_2_t2" />
                <equipment slot="Item1" id="Item.northern_round_shield" />
                <equipment slot="Head" id="Item.roughhide_cap" />
                <equipment slot="Cape" id="Item.scarf" />
                <equipment slot="Body" id="Item.nordic_sloven_leather" />
                <equipment slot="Gloves" id="Item.leather_gloves" />
                <equipment slot="Leg" id="Item.sturgia_boots_b" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t1" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_soldier']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_axe_3_t3" />
                <equipment slot="Item1" id="Item.northern_round_shield" />
                <equipment slot="Item2" id="Item.northern_spear_2_t3" />
                <equipment slot="Head" id="Item.nasal_helmet_with_mail" />
                <equipment slot="Cape" id="Item.empire_cape_a" />
                <equipment slot="Body" id="Item.leather_and_iron_plate_armor" />
                <equipment slot="Gloves" id="Item.highland_gloves" />
                <equipment slot="Leg" id="Item.wrapped_leather_boots" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.bastard_sword_t2" />
                <equipment slot="Item1" id="Item.northern_round_shield" />
                <equipment slot="Item2" id="Item.northern_spear_2_t3" />
                <equipment slot="Head" id="Item.nasal_helmet_with_leather" />
                <equipment slot="Cape" id="Item.stitched_leather_shoulders" />
                <equipment slot="Body" id="Item.northern_leather_tabard" />
                <equipment slot="Gloves" id="Item.rough_tied_bracers" />
                <equipment slot="Leg" id="Item.wrapped_leather_boots" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t2" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_spearman']/skills/skill[@id='Polearm']/@value">
        <xsl:attribute name="value">120</xsl:attribute>
    </xsl:template>
    <xsl:template match="NPCCharacter[@id='sturgian_spearman']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_3_t3" />
                <equipment slot="Item1" id="Item.viking_round_shield" />
                <equipment slot="Item2" id="Item.triangluar_spear_t3" />
                <equipment slot="Head" id="Item.goggled_helmet_over_leather" />
                <equipment slot="Cape" id="Item.a_brass_lamellar_shoulder_white_b" />
                <equipment slot="Body" id="Item.sturgian_chainmale_shortsleeve" />
                <equipment slot="Gloves" id="Item.northern_plated_gloves" />
                <equipment slot="Leg" id="Item.northern_plated_boots" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_3_t3" />
                <equipment slot="Item1" id="Item.viking_round_shield" />
                <equipment slot="Item2" id="Item.triangluar_spear_t3" />
                <equipment slot="Head" id="Item.sturgian_helmet_open" />
                <equipment slot="Cape" id="Item.mail_shoulders" />
                <equipment slot="Body" id="Item.sturgian_chainmale_shortsleeve" />
                <equipment slot="Gloves" id="Item.mail_mitten" />
                <equipment slot="Leg" id="Item.northern_plated_boots" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_3_t3" />
                <equipment slot="Item1" id="Item.viking_round_shield" />
                <equipment slot="Item2" id="Item.triangluar_spear_t3" />
                <equipment slot="Head" id="Item.sturgian_helmet_b_open" />
                <equipment slot="Cape" id="Item.a_brass_lamellar_shoulder_white_b" />
                <equipment slot="Body" id="Item.sturgian_lamellar_gambeson" />
                <equipment slot="Gloves" id="Item.mail_mitten" />
                <equipment slot="Leg" id="Item.northern_plated_boots" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t3" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_shock_troop']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_4_t4" />
                <equipment slot="Item1" id="Item.northern_spear_4_t5" />
                <equipment slot="Item2" id="Item.strapped_round_shield" />
                <equipment slot="Head" id="Item.goggled_helmet_over_full_mail" />
                <equipment slot="Body" id="Item.sturgian_chainmale_longsleeve" />
                <equipment slot="Cape" id="Item.brass_lamellar_shoulder_white" />
                <equipment slot="Gloves" id="Item.northern_plated_gloves" />
                <equipment slot="Leg" id="Item.mail_chausses" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_5_t5" />
                <equipment slot="Item1" id="Item.northern_spear_4_t5" />
                <equipment slot="Item2" id="Item.strapped_round_shield" />
                <equipment slot="Head" id="Item.sturgian_helmet_closed" />
                <equipment slot="Body" id="Item.sturgian_chainmale_longsleeve" />
                <equipment slot="Cape" id="Item.brass_lamellar_shoulder_white" />
                <equipment slot="Gloves" id="Item.mail_mitten" />
                <equipment slot="Leg" id="Item.mail_chausses" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_4_t4" />
                <equipment slot="Item1" id="Item.northern_spear_4_t5" />
                <equipment slot="Item2" id="Item.strapped_round_shield" />
                <equipment slot="Head" id="Item.sturgian_helmet_b_close" />
                <equipment slot="Body" id="Item.sturgian_lamellar_gambeson_heavy" />
                <equipment slot="Cape" id="Item.brass_lamellar_shoulder_white" />
                <equipment slot="Gloves" id="Item.northern_brass_bracers" />
                <equipment slot="Leg" id="Item.mail_chausses" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t2" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_veteran_warrior']/skills/skill[@id='OneHanded']/@value">
        <xsl:attribute name="value">140</xsl:attribute>
    </xsl:template>
    <xsl:template match="NPCCharacter[@id='sturgian_veteran_warrior']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_axe_4_t4" />
                <equipment slot="Item1" id="Item.heavy_round_shield" />
                <equipment slot="Item2" id="Item.northern_javelin_3_t4" />
                <equipment slot="Head" id="Item.closed_goggled_helmet" />
                <equipment slot="Body" id="Item.northern_coat_of_plates" />
                <equipment slot="Cape" id="Item.brass_scale_shoulders" />
                <equipment slot="Gloves" id="Item.northern_brass_bracers" />
                <equipment slot="Leg" id="Item.northern_plated_boots" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.battle_axe_t4" />
                <equipment slot="Item1" id="Item.heavy_round_shield" />
                <equipment slot="Item2" id="Item.northern_javelin_3_t4" />
                <equipment slot="Head" id="Item.lendman_helmet_over_full_mail" />
                <equipment slot="Body" id="Item.northern_coat_of_plates" />
                <equipment slot="Cape" id="Item.brass_lamellar_shoulder_white" />
                <equipment slot="Gloves" id="Item.northern_plated_gloves" />
                <equipment slot="Leg" id="Item.northern_plated_boots" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t3" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_berzerker']/skills/skill[@id='Throwing']/@value">
        <xsl:attribute name="value">80</xsl:attribute>
    </xsl:template>
    <xsl:template match="NPCCharacter[@id='sturgian_berzerker']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.bearded_axe_t3" />
                <equipment slot="Item1" id="Item.sturgia_sword_3_t3" />
                <equipment slot="Item2" id="Item.northern_throwing_axe_1_t1" />
                <equipment slot="Head" id="Item.goggled_helmet_over_mail" />
                <equipment slot="Body" id="Item.sturgian_lamellar_base" />
                <equipment slot="Gloves" id="Item.mail_mitten" />
                <equipment slot="Leg" id="Item.mail_chausses" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.northern_axe_t3" />
                <equipment slot="Item1" id="Item.sturgia_sword_3_t3" />
                <equipment slot="Item2" id="Item.northern_throwing_axe_1_t1" />
                <equipment slot="Head" id="Item.northern_goggled_helmet" />
                <equipment slot="Cape" id="Item.wrapped_scarf" />
                <equipment slot="Body" id="Item.sturgian_lamellar_base" />
                <equipment slot="Gloves" id="Item.mail_mitten" />
                <equipment slot="Leg" id="Item.mail_chausses" />
            </EquipmentRoster>
        </Equipments>
        <EquipmentSet id="sturgia_troop_civilian_template_t3" civilian="true" />
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_ulfhednar']/face">
        <face>
            <face_key_template value="BodyProperty.fighter_sturgia" />
        </face>
    </xsl:template>
    <xsl:template match="NPCCharacter[@id='sturgian_ulfhednar']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.northern_axe_t3" />
                <equipment slot="Item1" id="Item.sturgia_sword_4_t4" />
                <equipment slot="Item2" id="Item.northern_throwing_axe_1_t1" />
                <equipment slot="Head" id="Item.goggled_helmet_over_full_mail" />
                <equipment slot="Body" id="Item.sturgian_lamellar_fortified" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Gloves" id="Item.mail_mitten" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_2haxe_1_t4" />
                <equipment slot="Item1" id="Item.sturgia_sword_4_t4" />
                <equipment slot="Item2" id="Item.northern_throwing_axe_1_t1" />
                <equipment slot="Head" id="Item.closed_goggled_helmet" />
                <equipment slot="Cape" id="Item.wrapped_scarf" />
                <equipment slot="Body" id="Item.sturgian_lamellar_fortified" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Gloves" id="Item.mail_mitten" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t2" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_woodsman']/skills/skill[@id='Throwing']/@value">
        <xsl:attribute name="value">80</xsl:attribute>
    </xsl:template>
    <xsl:template match="NPCCharacter[@id='sturgian_woodsman']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_axe_2_t2" />
                <equipment slot="Item1" id="Item.northern_javelin_1_t2" />
                <equipment slot="Head" id="Item.northern_fur_cap" />
                <equipment slot="Cape" id="Item.wrapped_scarf" />
                <equipment slot="Body" id="Item.fur_trimmed_tunic" />
                <equipment slot="Gloves" id="Item.leather_gloves" />
                <equipment slot="Leg" id="Item.sturgia_boots_a" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_axe_2_t2" />
                <equipment slot="Item1" id="Item.northern_javelin_1_t2" />
                <equipment slot="Head" id="Item.roughhide_cap" />
                <equipment slot="Cape" id="Item.scarf" />
                <equipment slot="Body" id="Item.northern_leather_vest" />
                <equipment slot="Gloves" id="Item.leather_gloves" />
                <equipment slot="Leg" id="Item.sturgia_boots_b" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t1" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_hunter']/skills/skill[@id='Bow']/@value">
        <xsl:attribute name="value">120</xsl:attribute>
    </xsl:template>
    <xsl:template match="NPCCharacter[@id='sturgian_hunter']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.mountain_hunting_bow" />
                <equipment slot="Item1" id="Item.barbed_arrows" />
                <equipment slot="Item2" id="Item.barbed_arrows" />
                <equipment slot="Item3" id="Item.sturgia_axe_2_t2" />
                <equipment slot="Head" id="Item.spangenhelm_with_leather" />
                <equipment slot="Body" id="Item.nordic_sloven" />
                <equipment slot="Gloves" id="Item.padded_vambrace" />
                <equipment slot="Leg" id="Item.highland_boots" />
                <equipment slot="Cape" id="Item.hood" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.mountain_hunting_bow" />
                <equipment slot="Item1" id="Item.barbed_arrows" />
                <equipment slot="Item2" id="Item.barbed_arrows" />
                <equipment slot="Item3" id="Item.sturgia_sword_1_t2" />
                <equipment slot="Head" id="Item.spangenhelm_with_padded_cloth" />
                <equipment slot="Body" id="Item.nordic_sloven" />
                <equipment slot="Gloves" id="Item.guarded_armwraps" />
                <equipment slot="Leg" id="Item.highland_boots" />
                <equipment slot="Cape" id="Item.wrapped_scarf" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t2" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_archer']/skills/skill[@id='Bow']/@value">
        <xsl:attribute name="value">140</xsl:attribute>
    </xsl:template>
    <xsl:template match="NPCCharacter[@id='sturgian_archer']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.nordic_shortbow" />
                <equipment slot="Item1" id="Item.barbed_arrows" />
                <equipment slot="Item2" id="Item.barbed_arrows" />
                <equipment slot="Item3" id="Item.sturgia_axe_3_t3" />
                <equipment slot="Head" id="Item.nasal_helmet" />
                <equipment slot="Body" id="Item.northern_lamellar_armor" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Gloves" id="Item.guarded_padded_vambrace" />
                <equipment slot="Cape" id="Item.hood" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.nordic_shortbow" />
                <equipment slot="Item1" id="Item.barbed_arrows" />
                <equipment slot="Item2" id="Item.barbed_arrows" />
                <equipment slot="Item3" id="Item.sturgia_sword_3_t3" />
                <equipment slot="Head" id="Item.sturgian_helmet_base" />
                <equipment slot="Body" id="Item.northern_lamellar_armor" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Gloves" id="Item.rough_tied_bracers" />
                <equipment slot="Cape" id="Item.wrapped_scarf" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t2" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_archer']/skills/skill[@id='Bow']/@value">
        <xsl:attribute name="value">160</xsl:attribute>
    </xsl:template>
    <xsl:template match="NPCCharacter[@id='sturgian_veteran_bowman']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.nordic_shortbow" />
                <equipment slot="Item1" id="Item.bodkin_arrows_b" />
                <equipment slot="Item2" id="Item.bodkin_arrows_b" />
                <equipment slot="Item3" id="Item.sturgia_axe_3_t3" />
                <equipment slot="Head" id="Item.nasal_helmet_with_leather" />
                <equipment slot="Body" id="Item.nordic_sloven_over_mail" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Gloves" id="Item.northern_brass_bracers" />
                <equipment slot="Cape" id="Item.hood" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.nordic_shortbow" />
                <equipment slot="Item1" id="Item.bodkin_arrows_b" />
                <equipment slot="Item2" id="Item.bodkin_arrows_b" />
                <equipment slot="Item3" id="Item.sturgia_sword_3_t3" />
                <equipment slot="Head" id="Item.nasal_helmet_with_mail" />
                <equipment slot="Body" id="Item.nordic_sloven_over_mail" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Gloves" id="Item.northern_brass_bracers" />
                <equipment slot="Cape" id="Item.wrapped_scarf" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t3" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_brigand']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_1_t2" />
                <equipment slot="Item1" id="Item.northern_round_shield" />
                <equipment slot="Item2" id="Item.northern_javelin_1_t2" />
                <equipment slot="Item3" id="Item.northern_javelin_1_t2" />
                <equipment slot="Head" id="Item.nasalhelm_over_leather" />
                <equipment slot="Body" id="Item.northern_padded_cloth" />
                <equipment slot="Leg" id="Item.highland_boots" />
                <equipment slot="Cape" id="Item.scarf" />
                <equipment slot="Gloves" id="Item.highland_gloves" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_axe_2_t2" />
                <equipment slot="Item1" id="Item.northern_round_shield" />
                <equipment slot="Item2" id="Item.northern_javelin_1_t2" />
                <equipment slot="Item3" id="Item.northern_javelin_1_t2" />
                <equipment slot="Head" id="Item.nasalhelm_over_mail" />
                <equipment slot="Body" id="Item.northern_padded_gambeson" />
                <equipment slot="Leg" id="Item.highland_boots" />
                <equipment slot="Cape" id="Item.hood" />
                <equipment slot="Gloves" id="Item.guarded_armwraps" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t2" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_hardened_brigand']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_lance_1_t4" />
                <equipment slot="Item1" id="Item.viking_round_shield" />
                <equipment slot="Item2" id="Item.northern_javelin_2_t3" />
                <equipment slot="Item3" id="Item.sturgia_sword_3_t3" />
                <equipment slot="Head" id="Item.sturgian_helmet_open" />
                <equipment slot="Body" id="Item.nordic_lamellar_vest" />
                <equipment slot="Leg" id="Item.highland_boots" />
                <equipment slot="Gloves" id="Item.highland_gloves" />
                <equipment slot="Cape" id="Item.battania_civil_cape" />
                <equipment slot="Horse" id="Item.sturgia_horse" />
                <equipment slot="HorseHarness" id="Item.northern_noble_harness" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_lance_1_t4" />
                <equipment slot="Item1" id="Item.viking_round_shield" />
                <equipment slot="Item2" id="Item.northern_javelin_2_t3" />
                <equipment slot="Item3" id="Item.sturgia_axe_3_t3" />
                <equipment slot="Head" id="Item.sturgia_heavy_cavalary_helmet" />
                <equipment slot="Body" id="Item.nordic_lamellar_vest" />
                <equipment slot="Leg" id="Item.highland_boots" />
                <equipment slot="Gloves" id="Item.highland_gloves" />
                <equipment slot="Cape" id="Item.stitched_leather_shoulders" />
                <equipment slot="Horse" id="Item.sturgia_horse" />
                <equipment slot="HorseHarness" id="Item.northern_noble_harness" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t2" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_horse_raider']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_lance_1_t4" />
                <equipment slot="Item1" id="Item.leather_round_shield" />
                <equipment slot="Item2" id="Item.northern_javelin_3_t4" />
                <equipment slot="Item3" id="Item.sturgia_sword_5_t4" />
                <equipment slot="Head" id="Item.sturgian_helmet_closed" />
                <equipment slot="Cape" id="Item.stitched_leather_shoulders" />
                <equipment slot="Body" id="Item.nordic_lamellar_armor" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Gloves" id="Item.northern_plated_gloves" />
                <equipment slot="Horse" id="Item.t2_sturgia_horse" />
                <equipment slot="HorseHarness" id="Item.bandit_saddle_highland" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_lance_1_t4" />
                <equipment slot="Item1" id="Item.leather_round_shield" />
                <equipment slot="Item2" id="Item.northern_javelin_3_t4" />
                <equipment slot="Item3" id="Item.sturgia_axe_4_t4" />
                <equipment slot="Head" id="Item.sturgian_helmet_b_close" />
                <equipment slot="Cape" id="Item.battania_civil_cape" />
                <equipment slot="Body" id="Item.nordic_lamellar_armor" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Gloves" id="Item.mail_mitten" />
                <equipment slot="Horse" id="Item.t2_sturgia_horse" />
                <equipment slot="HorseHarness" id="Item.bandit_saddle_highland" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t3" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='sturgian_warrior_son']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_1_t2" />
                <equipment slot="Item1" id="Item.northern_round_shield" />
                <equipment slot="Head" id="Item.nordic_leather_cap" />
                <equipment slot="Cape" id="Item.wolf_shoulder" />
                <equipment slot="Body" id="Item.nordic_padded_cloth" />
                <equipment slot="Gloves" id="Item.ragged_armwraps" />
                <equipment slot="Leg" id="Item.sturgia_boots_a" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_1_t2" />
                <equipment slot="Item1" id="Item.northern_round_shield" />
                <equipment slot="Head" id="Item.nordic_fur_cap" />
                <equipment slot="Cape" id="Item.wolf_shoulder" />
                <equipment slot="Body" id="Item.northern_tunic" />
                <equipment slot="Gloves" id="Item.armwraps" />
                <equipment slot="Leg" id="Item.sturgia_boots_b" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t1" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='varyag']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.bastard_sword_t2" />
                <equipment slot="Item1" id="Item.northern_round_shield" />
                <equipment slot="Item2" id="Item.northern_throwing_axe_1_t1" />
                <equipment slot="Head" id="Item.nasalhelm_over_leather" />
                <equipment slot="Cape" id="Item.wolf_shoulder" />
                <equipment slot="Body" id="Item.decorated_nordic_hauberk" />
                <equipment slot="Leg" id="Item.highland_leg_wrappings" />
                <equipment slot="Gloves" id="Item.rough_tied_bracers" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_axe_3_t3" />
                <equipment slot="Item1" id="Item.northern_round_shield" />
                <equipment slot="Item2" id="Item.northern_throwing_axe_1_t1" />
                <equipment slot="Head" id="Item.nasalhelm_over_mail" />
                <equipment slot="Cape" id="Item.mail_shoulders" />
                <equipment slot="Body" id="Item.nordic_hauberk" />
                <equipment slot="Leg" id="Item.highland_leg_wrappings" />
                <equipment slot="Gloves" id="Item.highland_gloves" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t2" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='varyag_veteran']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_sword_4_t4" />
                <equipment slot="Item1" id="Item.viking_round_shield" />
                <equipment slot="Item2" id="Item.northern_throwing_axe_1_t1" />
                <equipment slot="Head" id="Item.northern_goggled_helmet" />
                <equipment slot="Cape" id="Item.bearskin" />
                <equipment slot="Body" id="Item.nordic_hauberk" />
                <equipment slot="Gloves" id="Item.northern_plated_gloves" />
                <equipment slot="Leg" id="Item.mail_chausses" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_axe_4_t4" />
                <equipment slot="Item1" id="Item.viking_round_shield" />
                <equipment slot="Item2" id="Item.northern_throwing_axe_1_t1" />
                <equipment slot="Head" id="Item.northern_goggled_helmet" />
                <equipment slot="Cape" id="Item.mail_shoulders" />
                <equipment slot="Body" id="Item.decorated_nordic_hauberk" />
                <equipment slot="Gloves" id="Item.mail_mitten" />
                <equipment slot="Leg" id="Item.mail_chausses" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t3" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='druzhinnik']/skills/skill[@id='Athletics']/@value">
        <xsl:attribute name="value">120</xsl:attribute>
    </xsl:template>
    <xsl:template match="NPCCharacter[@id='druzhinnik']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_lance_1_t4" />
                <equipment slot="Item1" id="Item.leather_round_shield" />
                <equipment slot="Item2" id="Item.sturgia_sword_4_t4" />
                <equipment slot="Head" id="Item.northern_warlord_helmet" />
                <equipment slot="Cape" id="Item.rough_bearskin" />
                <equipment slot="Body" id="Item.northern_brass_lamellar_over_mail" />
                <equipment slot="Gloves" id="Item.northern_plated_gloves" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Horse" id="Item.t2_sturgia_horse" />
                <equipment slot="HorseHarness" id="Item.chain_horse_harness" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_lance_1_t4" />
                <equipment slot="Item1" id="Item.leather_round_shield" />
                <equipment slot="Item2" id="Item.sturgia_axe_5_t5" />
                <equipment slot="Head" id="Item.lendman_helmet_over_mail" />
                <equipment slot="Cape" id="Item.brass_lamellar_shoulder_white" />
                <equipment slot="Body" id="Item.northern_brass_lamellar_over_mail" />
                <equipment slot="Gloves" id="Item.mail_mitten" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Horse" id="Item.t2_sturgia_horse" />
                <equipment slot="HorseHarness" id="Item.chain_horse_harness" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t3" civilian="true" />
        </Equipments>
    </xsl:template>

    <xsl:template match="NPCCharacter[@id='druzhinnik_champion']/skills/skill[@id='Polearm']/@value">
        <xsl:attribute name="value">200</xsl:attribute>
    </xsl:template>
    <xsl:template match="NPCCharacter[@id='druzhinnik_champion']/Equipments">
        <Equipments>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_lance_2_t5" />
                <equipment slot="Item1" id="Item.heavy_round_shield" />
                <equipment slot="Item2" id="Item.sturgia_sword_5_t5" />
                <equipment slot="Head" id="Item.lendman_helmet_over_full_mail" />
                <equipment slot="Cape" id="Item.rough_bearskin" />
                <equipment slot="Body" id="Item.sturgian_fortified_armor" />
                <equipment slot="Gloves" id="Item.northern_plated_gloves" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Horse" id="Item.t2_sturgia_horse" />
                <equipment slot="HorseHarness" id="Item.chain_horse_harness" />
            </EquipmentRoster>
            <EquipmentRoster>
                <equipment slot="Item0" id="Item.sturgia_lance_2_t5" />
                <equipment slot="Item1" id="Item.heavy_round_shield" />
                <equipment slot="Item2" id="Item.sturgia_sword_5_t4" />
                <equipment slot="Head" id="Item.closed_goggled_helmet" />
                <equipment slot="Cape" id="Item.brass_lamellar_shoulder_white" />
                <equipment slot="Body" id="Item.sturgian_fortified_armor" />
                <equipment slot="Gloves" id="Item.reinforced_mail_mitten" />
                <equipment slot="Leg" id="Item.mail_chausses" />
                <equipment slot="Horse" id="Item.t2_sturgia_horse" />
                <equipment slot="HorseHarness" id="Item.chain_horse_harness" />
            </EquipmentRoster>
            <EquipmentSet id="sturgia_troop_civilian_template_t3" civilian="true" />
        </Equipments>
    </xsl:template>
</xsl:stylesheet>